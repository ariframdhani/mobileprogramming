function teriak() {
  console.log("Halo Humanika!");
}

console.log(teriak()) //"Halo Humanika!"

function kalikan(num1_param, num2_param) {
  return num1_param * num2_param;
}

var num1 = 12;
var num2 = 4;

var hasilkali = kalikan(num1,num2);
console.log(hasilkali); // 48

function introduce(name_param, age_param, address_param, hobby_param) {
  console.log("Nama saya " + name_param + ", umur saya " + age_param + ", alamat saya di " + address_param + ", dan saya punya hobby yaitu " + hobby_param);
}

var name = "Arif Ramdhani";
var age = 21;
var address = "Jln. Desa Kamojing, Cikampek";
var hobby = "Futsal";

var fullSentence = introduce(name,age,address,hobby);
console.log(fullSentence); // Menampilkan "Nama saya Arif Ramdhani, umur saya 21 tahun, alamat saya di Jln. Desa Kamojing, dan saya punya hobby yaitu Futsal!"
